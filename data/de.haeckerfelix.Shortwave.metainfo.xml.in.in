<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
  <!-- Meson Build -->
  <id>@APP_ID@</id>
  <name>@NAME@</name>
  <translation type="gettext">@PKGNAME@</translation>
  <launchable type="desktop-id">@APP_ID@.desktop</launchable>

  <!-- General -->
  <summary>Listen to internet radio</summary>
  <description>
    <p>
      Shortwave is an internet radio player that provides access to a station database with over 50,000 stations.
    </p>
    <p>Features:</p>
    <ul>
      <li>Create your own library where you can add your favorite stations</li>
      <li>Easily search and discover new radio stations</li>
      <li>Automatic recognition of tracks, with the possibility to save them individually</li>
      <li>Responsive application layout, compatible for small and large screens</li>
      <li>Play audio on supported network devices (e.g. Google Chromecasts)</li>
      <li>Seamless integration into the GNOME desktop environment</li>
    </ul>
  </description>
  <content_rating type="oars-1.1" />

  <!-- Branding -->
  <developer_name>Felix Häcker</developer_name>
  <developer id="de.haeckerfelix">
    <name>Felix Häcker</name>
  </developer>
  <branding>
    <color type="primary" scheme_preference="light">#fcd85e</color>
    <color type="primary" scheme_preference="dark">#865e3c</color>
  </branding>

  <!-- License -->
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0-or-later</project_license>

  <!-- URLs -->
  <url type="homepage">https://apps.gnome.org/Shortwave/</url>
  <url type="bugtracker">https://gitlab.gnome.org/World/Shortwave/issues</url>
  <url type="donation">https://liberapay.com/haecker-felix</url>
  <url type="translate">https://l10n.gnome.org/module/Shortwave/</url>
  <url type="contact">https://matrix.to/#/#shortwave:gnome.org</url>
  <url type="vcs-browser">https://gitlab.gnome.org/World/Shortwave</url>
  <url type="contribute">https://welcome.gnome.org/app/Shortwave/</url>

  <!-- Supported Hardware -->
  <requires>
    <display_length compare="ge">360</display_length>
  </requires>
  <supports>
    <control>keyboard</control>
    <control>pointing</control>
    <control>touch</control>
  </supports>

  <!-- Screenshots -->
  <screenshots>
    <screenshot type="default">
      <image>https://gitlab.gnome.org/World/Shortwave/raw/main/data/screenshots/1.png</image>
      <caption>Create your own library where you can add your favorite stations</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.gnome.org/World/Shortwave/raw/main/data/screenshots/2.png</image>
      <caption>Easily search and discover new radio stations</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.gnome.org/World/Shortwave/raw/main/data/screenshots/3.png</image>
      <caption>Responsive application layout, compatible for small and large screens</caption>
    </screenshot>
  </screenshots>

  <!-- Releases -->
  <releases>
    <release version="5.0.0" date="2025-02-05">
      <description>
        <p>
          Shortwave now supports background playback, allowing the playback to continue when you close the window. If desired, the old behaviour can be restored in the preferences.
        </p>
        <p>
          The recording feature has also been completely overhauled. You can now choose from three different modes:
        </p>
        <ul>
          <li>Save All Tracks: Automatically save all recorded tracks</li>
          <li>Decide for Each Track: Temporarily record tracks and save only the ones you want</li>
          <li>Record Nothing: Stations are played without recording</li>
        </ul>
        <p>
          Additional Recording Enhancements:
        </p>
        <ul>
          <li>Directory in which recorded tracks are saved can be changed</li>
          <li>Minimum and maximum duration of recorded tracks can be configured</li>
          <li>Tracks can already be marked to be saved during recording</li>
          <li>Ongoing recordings can be cancelled</li>
          <li>Improved desktop notifications, with the direct option to mark tracks to be saved</li>
          <li>New dialog window for tracks with detailed information</li>
        </ul>
        <p>
          Other Improvements:
        </p>
        <ul>
          <li>Volume can now be changed via keyboard shortcuts</li>
          <li>Fixed an issue which prevented station covers from loading in certain desktop environments</li>
        </ul>
      </description>
    </release>
    <release version="4.0.1" date="2024-10-18">
      <description>
        <p>
          App metadata / branding improvements
        </p>
      </description>
    </release>
    <release version="4.0.0" date="2024-10-17">
      <description>
        <p>
          General
        </p>
        <ul>
          <li>New MPRIS media controls implementation with improved CPU usage</li>
          <li>Song notifications are disabled by default now</li>
          <li>Refreshed user interface by making use of new Libadwaita widgets</li>
          <li>Large parts of the app were reworked, providing a solid foundation for the next upcoming features</li>
        </ul>
        <p>
          Playback
        </p>
        <ul>
          <li>Last station now gets restored on app launch</li>
          <li>Redesigned player sidebar, allowing to control volume more easily</li>
          <li>New recording indicator showing whether the current playback is being recorded</li>
          <li>Fixed buffering issue which prevented playing new stations, especially after switching stations too fast</li>
          <li>Fixed issues which sometimes prevented that a song gets recorded</li>
          <li>Fixed issue that volume remains muted after unmuting</li>
        </ul>
        <p>
          Library
        </p>
        <ul>
          <li>No more loading on startup, stations now get directly retrieved from cached data</li>
          <li>Fixed issue which sometimes prevented loading more than 8 stations from library</li>
        </ul>
        <p>
          Station Covers
        </p>
        <ul>
          <li>More supported image file format for station covers</li>
          <li>Enhanced security by loading station covers using sandboxed Glycin image library</li>
          <li>Non square covers automatically get a blurred background</li>
          <li>New generated fallback for stations without any cover image</li>
          <li>Improved disk usage by automatically purging no longer needed cached data</li>
        </ul>
        <p>
          Browse / Search
        </p>
        <ul>
          <li>More useful station suggestions by respecting configured system language / region</li>
          <li>Suggestions now get updated with every start, no longer always showing the same stations</li>
          <li>More accessible search feature, no longer hidden in a subpage</li>
          <li>Search results are no longer limited at 250 stations</li>
          <li>Faster and more efficient search by using new grid widgets</li>
        </ul>
        <p>
          Chromecast
        </p>
        <ul>
          <li>Shortwave is now a registered Google Cast app, no longer relying on the generic media player</li>
          <li>New backend which greatly improves communication stability with cast devices</li>
          <li>Improved discovery of cast devices with lower CPU and memory usage</li>
          <li>Now possible to change the volume of a connected cast device</li>
        </ul>
      </description>
    </release>
    <release version="3.2.0" date="2023-02-07">
      <description>
        <p>
          This version includes minor improvements and bug fixes.
        </p>
        <ul>
          <li>Small user interface improvements</li>
          <li>App can now be closed with CTRL+W</li>
          <li>Updated translations</li>
        </ul>
      </description>
    </release>
    <release version="3.1.0" date="2022-10-15">
      <description>
        <p>
          This version includes minor improvements and bug fixes.
        </p>
        <ul>
          <li>Small user interface improvements</li>
          <li>Use new Adwaita about dialog</li>
          <li>Fixed a crash when the "Music" directory is not available</li>
          <li>Updated translations</li>
        </ul>
      </description>
    </release>
    <release version="3.0.0" date="2022-04-23">
      <description>
        <p>
          Version 3.0.0 is a stable release, with the following major
          improvements since 2.0.1:
        </p>
        <ul>
          <li>Updated user interface that uses the new Adwaita design, with many improvements.</li>
          <li>Support for the new GNOME 42 dark mode.</li>
          <li>New option to add private stations to the library, which should not (or cannot) be available on radio-browser.info (e.g. local network or paid streams).</li>
          <li>Display station bitrate information, which can also be used as a sorting option.</li>
          <li>Save station data to disk so that it is still available when a station gets removed from the online database.</li>
          <li>New button on the search page that allows to sort the search results.</li>
          <li>Overhauled station dialog, with more clearly displayed information.</li>
          <li>Update desktop notification on song change instead of generating a new separate notification for each song.</li>
          <li>Shortwave can now be used normally, even if radio-browser.info is offline / unavailable.</li>
        </ul>
        <p>
          Besides these improvements, this version also includes many changes
          under the hood changes, so many crashes and other bugs have been
          fixed. It also contains updated translations for 31 different
          languages.
        </p>
      </description>
    </release>
    <release version="2.0.1" date="2021-04-23">
      <description>
        <p>
          This version contains minor improvements and bug fixes.
        </p>
        <ul>
          <li>Improve buffering to avoid audio issues for streams with a high bitrate.</li>
          <li>Fix issue that no connection could be established when a VPN connection was active.</li>
          <li>Automatically select search entry when the search view gets activated.</li>
        </ul>
      </description>
    </release>
    <release version="2.0.0" date="2021-03-10">
      <description>
        <p>
          Version 2.0.0 is a stable release, with the following major
          improvements since 1.1.0:
        </p>
        <ul>
          <li>Get notified about new songs via desktop notifications.</li>
          <li>New mini player window mode. Control important features with a super compact window widget.</li>
          <li>Redesigned station information dialog window. Added ability to copy the stream url.</li>
          <li>Improved keyboard navigation of the user interface.</li>
          <li>Inhibit sleep/hibernate mode during audio playback.</li>
        </ul>
        <p>
          Besides these improvements, this version contains many under the hood
          changes. The complete user interface has been updated from GTK3 to
          GTK4. Many app components have been modernised or completely
          rewritten. As a result, many crashes or other bugs have been fixed.
        </p>
        <p>
          This version also contains updated translations for 27 different
          languages.
        </p>
      </description>
    </release>
    <release version="1.1.1" date="2020-06-07">
      <description>
        <p>This version fixes a bug that caused some users not to be able to record songs.</p>
        <p>This release also updates translations in several languages.</p>
        <p>
          This might be the last release with the feature to import Gradio
          databases. The needed API server will be deactivated / no longer
          available in August 2020.
        </p>
      </description>
    </release>
    <release version="1.1.0" date="2020-06-01">
      <description>
        <p>
          Version 1.1.0 is a stable release, with the following major
          improvements since 1.0.1:
        </p>
        <ul>
          <li>Add CTRL + spacebar as shortcut to start/stop audio playback.</li>
          <li>Add stream buffering to prevent playback stuttering.</li>
          <li>Add featured carousel to discover page.</li>
          <li>Add support for non pulseaudio systems / environments.</li>
          <li>Add support for additional mouse buttons to switch between views.</li>
          <li>Improve stability of audio/recording backend.</li>
          <li>Fixed issue that the volume didn't get calculated correctly (linear/cubic).</li>
          <li>Fixed issue that broken stations are getting displayed in search results.</li>
          <li>Fixed issue with random scrolling in search page.</li>
          <li>Fixed issues which prevented to import Gradio databases.</li>
        </ul>
        <p>This release also updates translations in several languages.</p>
        <p>
          This might be the last release with the feature to import Gradio
          databases. The needed API server will be deactivated / no longer
          available in August 2020.
        </p>
      </description>
    </release>
    <release version="1.0.1" date="2020-03-15">
      <description>
        <p>Fixes a bug that no songs can be saved.</p>
        <p>Add Finnish translation.</p>
      </description>
    </release>
    <release version="1.0.0" date="2020-03-14">
      <description>
        <p>First stable release.</p>
      </description>
    </release>
    <release version="0.0.3" date="2020-03-08">
      <description>
        <p>Third beta release.</p>
      </description>
    </release>
    <release version="0.0.2" date="2020-02-17">
      <description>
        <p>Second beta release.</p>
      </description>
    </release>
    <release version="0.0.1" date="2019-11-09">
      <description>
        <p>First beta release.</p>
      </description>
    </release>
  </releases>
</component>